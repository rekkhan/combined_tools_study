#!/bin/bash


function maxbg
{
	nmaxbg=$1
	ncurbg=$(jobs -pr | wc -w)
	while [ $ncurbg -ge $nmaxbg ]; do
		sleep 1
		ncurbg=$(jobs -pr | wc -w)
	done
}


function show_help
{
	echo " Syntax:"
	echo "     run_fit.sh [-p|-h]"
	echo "     run_fit.sh [mode]"
	echo " Options:"
	echo "     -h: Show helps"
	echo "     -p: update the list of command (local) or create log folders (server)"
	echo " Mode:"
	echo "     simple   Fit with normal RooFit"
	echo "     mcstat   Fit with MC stat uncertainty"
}


function prepare_step
{
	local current_host=$( echo $HOSTNAME )
	echo " Working server detected [ $current_host ], create log folder"
	mkdir -p "$LOG_MAIN_DIR/"
	echo " |-- Created: [ $LOG_MAIN_DIR/ ]"
	echo " Done. Exit now."
	exit
}


function fill_card
{
	local card=$1
	local fit_mode=$2
	local unc_bin=$3
	local rootfile=$( printf "uncbin_%03d.root" $unc_bin )
	echo "imax *" > $card
	echo "jmax *" >> $card
	echo "kmax *" >> $card
	echo "--------------------" >> $card
	echo "shapes  sig       c1  $rootfile  signal" >> $card
	echo "shapes  bkg       c1  $rootfile  background" >> $card
	echo "shapes  data_obs  c1  $rootfile  data" >> $card
	echo "--------------------" >> $card
	echo "bin          c1" >> $card
	echo "observation  -1" >> $card
	echo "--------------------" >> $card
	echo "bin          c1     c1" >> $card
	echo "process      sig    bkg" >> $card
	echo "process      0      1" >> $card
	echo "rate         -1     -1" >> $card
	echo "--------------------" >> $card
	if [[ "$fit_mode" = "mcstat" ]]; then
		echo "c1 autoMCStats 6000 1" >> $card
	fi
}

function run_fitter
{
	local fit_mode=$1
	local unc_bin=$2
	local datacard=$( printf "datacard_${fit_mode}_%03d.txt" $unc_bin )
	echo "----- Create datacard: $datacard -----"
	fill_card $datacard $fit_mode $unc_bin
	echo ""
	local workspace=$( printf "workspace_${fit_mode}_%03d.root" $unc_bin )
	local phy_model="-P HiggsAnalysis.CombinedLimit.PhysicsModel:multiSignalModel"
	echo "----- Create workspace -----"
	text2workspace.py $datacard -o $workspace $phy_model --PO verbose \
		--PO 'map=.*/sig:mu1[200000,0,1000000]' --PO 'map=.*/bkg:mu2[800000,0,1000000]'
	echo ""
	local fitws_name=$( printf "_${fit_mode}_%03d" $unc_bin )
	local combine_arg1="-M MultiDimFit"
	local combine_arg2="--saveFitResult --saveNLL --robustFit on"
	echo "----- Run fit by combine tool -----"
	combine $combine_arg1 -n $fitws_name -d $workspace $combine_arg2
	echo ""

	rm $datacard
	local outworkspace=$( printf "higgsCombine_%s_%03d.MultiDimFit.mH120.root" \
		$fit_mode $unc_bin )
	rm $outworkspace
	rm $workspace
}


function para_proc
{
	local fit_mode=$1
	local code_dir="$PWD"
	local log_dir="$LOG_MAIN_DIR"
	mkdir -p $log_dir
	local work_dir="$WORK_MAIN_DIR"
	if [[ ! -d $work_dir ]]; then exit; fi
	cd $work_dir
	for unc_bin in {0..19}; do
		local log=$( printf "$log_dir/uncbin%03d_${fit_mode}.log" $unc_bin )
		echo " ---------- Unc-bin: $unc_bin ----------" > $log
		echo " Create datacards and do fitting "
		run_fitter $fit_mode $unc_bin &> $log &
		maxbg 20
	done
	wait

	cd $code_dir
	local log_sum="${log_dir}/summary_${fit_mode}.log"
	root -l -b -q result.C\(\"$fit_mode\"\) > $log_sum
}


echo " ============================="
echo "  Program running [ Toy fit ]"
echo " ============================="
MAIN_DIR="~/ROOT_Work/Task_ZgQCD" #Change this directory to your working directory
LOG_MAIN_DIR="$MAIN_DIR/CombinedToolsStudy/Log_ToyStudy/Fit"
WORK_MAIN_DIR="$MAIN_DIR/CombinedToolsStudy/utput/Histogram"
while getopts ":hp" option; do
	case $option in
		h) # Display help
			show_help
			exit;;
		p) # Print the updated command list
			prepare_step
			exit;;
	esac
done

mode=$1
if [[ $mode != "simple" && $mode != "mcstat" ]]; then
	echo "Unsuported mode: $mode"
	exit
fi
timeout=$( (time para_proc $mode > /dev/null) 2>&1 )
timetext=( )
timevalue=( )
count=0
for info in ${timeout[@]}; do
	res=$(( count%2 ))
	if [ $res -eq 0 ]; then timetext+=($info); else timevalue+=($info); fi
	count=$((count+1))
done

echo " [+] Total processing time:"
for idx in ${!timetext[@]}; do
	printf "  |-- %-5s  %s\n" "${timetext[idx]}:" "${timevalue[idx]}"
done
echo " [*] All tasks done. Exit now."
