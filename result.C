#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <string.h>

#include "TFile.h"
#include "TH1D.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TGaxis.h"
#include "TLatex.h"
#include "TLegend.h"
#include "RooFitResult.h"
#include "RooRealVar.h"

#include "Plugin/function.h"


double ratio_of_pad = 7.5 / 2.5;
void setup_1D ( TH1 *hist,
		int hcolour, int fill_style,
		int line_width, bool is_top = true )
{
	TGaxis :: SetMaxDigits ( 4 );
	TGaxis :: SetExponentOffset ( -0.10, 0.02, "y" );
	double scaleXtext = double( !is_top );
	hist -> SetFillColor   ( hcolour );
	hist -> SetFillStyle   ( fill_style );
	hist -> SetMarkerColor ( hcolour );
	hist -> SetMarkerStyle ( 20 );
	hist -> SetLineColor   ( hcolour );
	hist -> SetLineWidth   ( line_width );
	hist -> SetMarkerSize  ( 0.8 );
	hist -> GetXaxis( ) -> SetTitleSize   ( 0.050*scaleXtext );
	hist -> GetXaxis( ) -> SetLabelSize   ( 0.040*scaleXtext );
	hist -> GetYaxis( ) -> SetTitle ( "#events" );
	hist -> GetYaxis( ) -> SetMaxDigits   ( 4 );
	hist -> GetYaxis( ) -> SetTitleSize   ( 0.050 );
	hist -> GetYaxis( ) -> SetTitleOffset ( 0.350*ratio_of_pad );
	hist -> GetYaxis( ) -> SetLabelSize   ( 0.040 );
	hist -> GetYaxis( ) -> SetLabelOffset ( 0.006 );
}

TLatex* text_on_plot(
		bool ndc=true, int text_font=42, double text_size=0.05,
		int text_align=11, double text_angle=0 )
{
	TLatex *tex = new TLatex( );
	tex -> SetNDC( ndc );
	tex -> SetTextFont( text_font );
	tex -> SetTextSize( text_size );
	tex -> SetTextAlign( text_align );
	tex -> SetTextAngle( text_angle );
	return tex;
}

TPad *custom_pad (
		const char* pad_name,
		double x1, double y1, double x2, double y2,
		double margin_left, double margin_right,
		double margin_top, double margin_bottom,
		bool grid_x=false, bool grid_y=false )
{
	TPad *pad = new TPad( pad_name, "", x1, y1, x2, y2 );
	pad -> SetLeftMargin( margin_left );
	pad -> SetRightMargin( margin_right );
	pad -> SetTopMargin( margin_top );
	pad -> SetBottomMargin( margin_bottom );
	pad -> SetGrid( grid_x, grid_y );
	return pad;
}


double mean_from_vector ( std::vector<double> values )
{
	double sum = 0;
	int current_index = values.size( );
	while ( current_index-- )
		sum += values[current_index];
	return sum / values.size( );
}

double stddev_from_vector ( double mean, std::vector<double> values )
{
	double sum = 0;
	int current_index = values.size( );
	while ( current_index-- )
		sum += pow( values[current_index] - mean, 2 );
	return sqrt( sum / values.size( ) );
}


int result ( const char* fit_mode )
{
	gStyle -> SetOptStat( 0 );

	std::string dir_input = "Output/Histogram/";
	printf( " [+] Browse files in: %s\n", dir_input.data( ) );

	std::string dir_output = "Output/Result/";
	system( Form( "mkdir -p %s", dir_output.data( ) ) );
	std::string path_txt = Form( "%s/fit_summary_%s.txt", dir_output.data( ), fit_mode );
	FILE *file_txt = fopen( path_txt.data( ), "w" );
	fprintf ( file_txt, "%12s | %10s  %10s  %12s  %12s | %10s  %10s  %12s  %12s\n",
			"rel.unc[%]", "sig. yield", "sig. error", "sig. l-error", "sig. h-error",
			"bkg. yield", "bkg. error", "bkg. l-error", "bkg. h-error" );

	int n_files = 20;
	std::vector<double> fitted_values;
	for ( unsigned int unc_bin=0; unc_bin<n_files; unc_bin++ )
	{
		std::string path = Form (
				"%s/multidimfit_%s_%03d.root", dir_input.data( ), fit_mode, unc_bin );
		TFile *file = new TFile( path.data( ), "read" );
		if ( file->IsZombie( ) )
		{
			printf( "  |-- Not found: %s\n", path.data( ) );
			continue;
		}
		RooFitResult *paraset = ( RooFitResult* )file -> Get( "fit_mdf" );
		RooRealVar *par_mu1 = (RooRealVar*)paraset -> floatParsFinal( ) . find( "mu1" );
		double mu1_val = par_mu1 -> getValV( );
		double mu1_err = par_mu1 -> getError( );
		double mu1_errlo = par_mu1 -> getErrorLo( );
		double mu1_errhi = par_mu1 -> getErrorHi( );
		RooRealVar *par_mu2 = (RooRealVar*)paraset -> floatParsFinal( ) . find( "mu2" );
		double mu2_val = par_mu2 -> getValV( );
		double mu2_err = par_mu2 -> getError( );
		double mu2_errlo = par_mu2 -> getErrorLo( );
		double mu2_errhi = par_mu2 -> getErrorHi( );

		double multiplier = pow( 0.25, 1.0/1.5 ) / 20.0;
		double target_uncertainty = pow( unc_bin*multiplier, 1.5 ) + 0.005;
		fprintf( file_txt,
				"%12.2f | %10.0f  %10.0f  %12.0f  %12.0f | %10.0f  %10.0f  %12.0f  %12.0f\n",
				target_uncertainty*100,
				mu1_val, mu1_err, mu1_errlo, mu1_errhi,
				mu2_val, mu2_err, mu2_errlo, mu2_errhi );
		fitted_values . push_back( mu1_val );
		file -> Close( );
	}
	fclose( file_txt );
	printf( "  `-- Result is stored in [ %s ]\n", path_txt.data( ) );

	return 0;
}
