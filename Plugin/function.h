#define sqrt2pi 2.506628
#ifndef FUNCTION_H
#define FUNCTION_H

double pdf_model ( double* var, double* par )
{
	double variable = var[0];
	if ( !par[1] && !par[3] )   return 0;
	if ( !par[0] && !par[2] )   return 0;
	double bkg_slope  = fabs( par[0] );
	double sig_slope  = fabs( par[2] );
	double bkg_norm   = bkg_slope / ( 1-exp(-2*bkg_slope) );
	double sig_norm   = sig_slope / ( 1-exp(-2*sig_slope) );
	double func_scale = par[1] + par[3];
	double bkg_scale  = par[1] * bkg_norm / func_scale;
	double sig_scale  = par[3] * sig_norm / func_scale;
	double f_bkg = bkg_scale * exp( -bkg_slope * ( variable+1.0 ) );
	double f_sig = sig_scale * exp( sig_slope * ( variable-1.0 ) );
	if ( bkg_slope*sig_slope != 0 )
		return ( fabs(variable)<=1 ) * ( f_bkg + f_sig );
	if ( bkg_slope == 0 )
		return ( fabs(variable)<=1 ) * ( f_sig );
	return ( fabs(variable)<=1 ) * ( f_bkg );
}

double pdf_gauss ( double* var, double* par )
{
	double variable = var[0];
	double mean = par[0];
	double sigma = par[1];
	double scale = par[2];
	return exp( -0.5 * pow( (variable-mean)/sigma, 2 ) ) * scale;
}

#endif
