#!/bin/bash


function maxbg
{
	nmaxbg=$1
	ncurbg=$(jobs -pr | wc -w)
	while [ $ncurbg -ge $nmaxbg ]; do
		sleep 1
		ncurbg=$(jobs -pr | wc -w)
	done
}


function show_help
{
	echo " Syntax:"
	echo "     run_toygen.sh [-p|-h]"
	echo " Options:"
	echo "     -h: Show helps"
	echo "     -p: update the list of command (local) or create log folders (server)"
}


function prepare_step
{
	local current_host=$( echo $HOSTNAME )
	if [[ "$current_host" =~ .*cms.phy.ncu.edu.tw ]]; then
		echo " Working server detected [ $current_host ], create log folder"
		mkdir -p "$LOG_MAIN_DIR/"
		echo " |-- Created: [ $LOG_MAIN_DIR/ ]"
		echo " Done. Exit now."
		exit
	fi

	local cmd_list="cmdlist_toygen.txt"
	echo " Local machine detected [ $current_host ]"
	echo " Now printing the updated list of commands to [ $cmd_list ]"
	echo "===== LIST OF COMMAND: TOY GENERATOR =====" > $cmd_list
	local scriptname='./run_toygen.sh'
	local log="$LOG_MAIN_DIR/run.log"
	local cmd="$scriptname &> $log &"
	echo $cmd >> $cmd_list
	echo " Done. Exit now."
}


function para_proc
{
	local log_dir="$LOG_MAIN_DIR/"
	mkdir -p $log_dir
	for unc_bin in {0..19}; do
		local log=$( printf "$log_dir/uncbin_%03d.log" $unc_bin )
		root -l -b -q toygen.C\($unc_bin\) &> $log &
		maxbg 20
	done
	wait
}


echo " ==================================="
echo "  Program running [ Toy generator ]"
echo " ==================================="
LOG_MAIN_DIR="Log_ToyStudy/ToyGen"
while getopts ":hp" option; do
	case $option in
		h) # Display help
			show_help
			exit;;
		p) # Print the updated command list
			prepare_step
			exit;;
	esac
done

timeout=$( (time para_proc > /dev/null) 2>&1 )
timetext=( )
timevalue=( )
count=0
for info in ${timeout[@]}; do
	res=$(( count%2 ))
	if [ $res -eq 0 ]; then timetext+=($info); else timevalue+=($info); fi
	count=$((count+1))
done

echo " [+] Total processing time:"
for idx in ${!timetext[@]}; do
	printf "  |-- %-5s  %s\n" "${timetext[idx]}:" "${timevalue[idx]}"
done
echo " [*] All tasks done. Exit now."
