# A private package to study the autoMC option in the combined tools

This package create toys samples (histograms) to study the fit provided by the combined tools.

## To create the toys, run:
```
chmod +x run_toy.sh
./run_toy.sh -p #To create the log folder
./run_toy.sh
```
The toys distributions are stored in Output/Histogram/

## To run the fit
```
chmod +x run_fit.sh
./run_fit -p #To create the log folder
./run_fit.sh simple #To fit using RooFit
./run_fit.sh mcstat #To fit using the AutoMCStat feature
```
The fit results are summarized in Output/Result/fit_summary_*.txt